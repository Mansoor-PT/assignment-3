
package assignment.pkg3;


public class Objectpassing {
  double height,width,length;
 public Objectpassing(Objectpassing b) {
        height = b.height;
        width  = b.width;
        length = b.length;
    }
    public Objectpassing(double height, double width, double length) {
        this.height = height;
        this.width = width;
        this.length = length;
    }
  
    public double volume(){
        double volume=height*width*length;
        return volume;
    }
}
class Test{
    public static void main(String[] args) {
        Objectpassing obj=new Objectpassing(10,20,30);
        Objectpassing obj1=new Objectpassing(obj);
        double volume=obj.volume();
        System.out.println("the volume is of first object is:"+volume);
        volume=obj1.volume();
         System.out.println("the volume is of after passing first object is:"+volume);
      
    }
    
    
}
