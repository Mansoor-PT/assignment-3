
import java.lang.annotation.Annotation;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Inherited

@interface Smartphone
{
    String os();
    int version();
}

@Smartphone(os="Android",version=6)
class Nokia{
    String model;
    int size;

    public Nokia(String model, int size) {
        this.model = model;
        this.size = size;
    }
}

public class AnnotationCreation {
    public static void main(String[] args) {
        
    
    Nokia obj=new Nokia("Fire",7);
    Class c=obj.getClass();
    Annotation an=c.getAnnotation(Smartphone.class);
    Smartphone s=(Smartphone)an;
    System.out.println("operating system is:"+s.os()+", version of os is:"+s.version());
    
    
    }
    
   
            
    
}
