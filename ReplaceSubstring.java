
package assignment.pkg3;

import java.util.Scanner;

public class ReplaceSubstring {
    public static void main(String[] args) {
        System.out.println("Enter the string ");
        Scanner sc=new Scanner(System.in);
        String str1=sc.nextLine();
        String replaced=str1.replaceAll("hi", "hello");
        System.out.println("original string:"+ str1+"\n"+"replaced string: "+replaced);
    }
    
}
