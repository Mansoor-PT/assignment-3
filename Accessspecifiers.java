
public class Accessspecifiers {
    
    public static void main(String[] args) {
        A a =new A();
        a.test5();
        a.test2();
        a.test3();
        a.test4();
    }
    
}
class A{
        private void test1()
                 {
                 System.out.println("i am private");
                }
         public void test2()
                {
                    System.out.println("i am public");
                }
        void test4()
                {
                  System.out.println("i am default");
                }
      
         protected void test3()
                   {
                  System.out.println("i am protected");
         
                }   
         void test5(){
             A a=new A();
             a.test1();
         }
}